package org.springpaul.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by zmk523@163.com on 2020/4/18 14:39
 */
@SpringBootApplication
@EnableDiscoveryClient
public class PaulGateWayApplication {
    public static void main(String[] args) {
        SpringApplication.run(PaulGateWayApplication.class, args);
    }
}
