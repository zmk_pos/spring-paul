package org.springpaul.turbine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;


/**
 * Created by zmk523@163.com on 2020/4/21 9:02
 */

@SpringBootApplication
@EnableTurbine
@EnableHystrixDashboard
@EnableDiscoveryClient
public class PaulTurbineApplication {
    public static void main(String[] args) {
        SpringApplication.run(PaulTurbineApplication.class, args);
    }
}
