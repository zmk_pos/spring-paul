package org.springpaul.common.provider;

/**
 * Created by zmk523@163.com on 2020/4/25 17:06
 */

import lombok.Data;

/**
 * TokenInfo
 *
 * @author Chill
 */
@Data
public class TokenInfo {

    /**
     * 令牌值
     */
    private String token;

    /**
     * 过期秒数
     */
    private int expire;

}