package org.springpaul.common.constant;

/**
 * Created by zmk523@163.com on 2020/4/22 18:25
 */
public interface AppConstant {
    String APPLICATION_VERSION = "1.0.0";
}
