package org.springpaul.common.constant;

/**
 * Created by zmk523@163.com on 2020/4/22 17:58
 */
public interface TokenConstant {
    String SIGN_KEY = "paul";
    String HEADER = "Authorization";
    String BEARER = "Bearer ";
    String ACCESS_TOKEN = "access_token";
    String REFRESH_TOKEN = "refresh_token";
    String TOKEN_TYPE = "token_type";
    String ACCOUNT = "account";    
    String USER_ID = "user_id";
    String ROLE_ID = "role_id";
    String USER_NAME = "user_name";
    String ROLE_NAME = "role_name";
    String TENANT_ID = "tenant_id";
    String CLIENT_ID = "client_id";
    String LICENSE_NAME = "powered by paul";
}
