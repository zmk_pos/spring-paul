package org.springpaul.common.utils;
import org.springframework.lang.Nullable;

/**
 * 对象工具类
 *
 * Created by zmk523@163.com on 2020/4/25 15:45
 */
public class ObjectUtil extends org.springframework.util.ObjectUtils {

    /**
     * 判断元素不为空
     * @param obj object
     * @return boolean
     */
    public static boolean isNotEmpty(@Nullable Object obj) {
        return !ObjectUtil.isEmpty(obj);
    }

}
