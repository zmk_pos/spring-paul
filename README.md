一、项目介绍

    Spring Cloud版本：Hoxton.SR2
    Spring Boot版本： 2.2.5.RELEASE
    
二、项目介绍

    XXXX
        
三、相关技术

    Spring Cloud（分布式微服务框架）: https://projects.spring.io/spring-cloud/
    Spring Boot（快速应用开发Spring框架）：https://spring.io/projects/spring-boot/
    Ratelimit（网关限流框架）：https://github.com/marcosbarbero/spring-cloud-zuul-ratelimit/
    OAuth2（Oauth2认证服务）：https://spring.io/projects/spring-security-oauth/
    Spring session（分布式Session管理）：http://projects.spring.io/spring-session/
    MyBatis（ORM框架）：http://www.mybatis.org/mybatis-3/zh/index.html/
    MyBatis Generator（代码生成）：http://www.mybatis.org/generator/index.html/
    PageHelper（MyBatis物理分页插件）：http://git.oschina.net/free/Mybatis_PageHelper
    Druid（数据库连接池）：https://github.com/alibaba/druid/
    Sharding-JDBC（分布式数据库中间件）：http://shardingsphere.io/
    Fastdfs（轻量级分布式文件系统）：https://github.com/happyfish100/fastdfs/
    Redis（分布式缓存数据库）：https://redis.io/
    Swagger2（接口测试框架）：http://swagger.io/
    Maven（项目构建管理）：http://maven.apache.org/
    Spring Boot Admin（分布式微服务监控中心）：https://github.com/codecentric/spring-boot-admin/
    Hystrix-dashboard（Hystrix的仪表盘组件）：https://github.com/spring-cloud-samples/hystrix-dashboard/
    Turbine（Hystrix熔断聚合组件 ）：https://github.com/spring-cloud-samples/turbine/
    Zipkin（分布式链路跟踪系统）：https://zipkin.io/
    Kafka/RabbitMQ（消息中间件）：http://kafka.apache.org https://www.rabbitmq.com/
    easypoi（Excel快速POI工具类）：https://gitee.com/lemur/easypoi/
    sentinel(alibaba 流控防护):https://github.com/alibaba/Sentinel/
        
四、脚手架模块说明
    
    paul-gateway：平台网关
    paul-auth:平台授权
    paul-eureka：平台服务注册于发现
    paul-config：配置中心
    paul-sleuth：平台链路跟踪
    paul-turbine：聚合服务监控
    paul-admin：应用监控监控
    kjs：业务模块
    
    
五、项目运行    
     
    以此启动步骤
    paul-eureka
    paul-admin
    paul-config
    paul-gateway
    kjs-order-service
    kjs-product-service
    kjs-merge
    (
    说明-运行环境基础：
       ali sentinel：http://edas-public.oss-cn-hangzhou.aliyuncs.com/install_package/demo/sentinel-dashboard.jar
       openzipkin: https://dl.bintray.com/openzipkin/maven/io/zipkin/java/zipkin-server/
       ############注意版本#############
    )
       
    
    
六、参考文档
    
    ###框架搭建
    https://blog.csdn.net/weixin_43481793/article/details/99589752
    https://blog.csdn.net/weixin_43481793/article/details/99622085
    
    ###apigateway
    聚合服务调用演示地址：http://192.168.30.35:5555/demo/get/611562590585229320
    服务调用演示地址：http://192.168.30.35:5555/terminal-service/terminal/get/611562590585229320
    
    ###解决springboot 引用Eureka导致结果有json变为xml解决方案
    https://blog.csdn.net/zyb2017/article/details/80265070?utm_source=blogxgwz4
    
    ###在spring cloud项目中使用@ControllerAdvice做自定义异常拦截无效解决方案
    https://blog.csdn.net/weixin_33943836/article/details/86022557
    https://blog.csdn.net/qq_24267619/article/details/78449160
    
    ###SpringBoot 返回json数据日期格式化 两行设置解决
    https://blog.csdn.net/wxb880114/article/details/83413436【可行】
    https://www.cnblogs.com/bcde/p/9679053.html
    
    ###springcloud json fasterxml date 日期格式 设置
    https://blog.csdn.net/thomescai/article/details/80019641【可行】
    https://blog.csdn.net/bigtree_3721/article/details/70788746
    
    ###springcloud hystrix 熔断 常见问题
    https://blog.csdn.net/Lcxy_Demo/article/details/88030862
    
    ### springcloud   feign和ribbon 远程调用比较
    feign集成ribbon使用，其他自行百度
    
    ### springcloud config 
    https://www.cnblogs.com/huangjuncong/p/9069749.html
    
    onfig支持我们使用的请求的参数规则为：
    
    / { 应用名 } / { 环境名 } [ / { 分支名 } ]
    / { 应用名 } - { 环境名 }.yml
    / { 应用名 } - { 环境名 }.properties
    / { 分支名 } / { 应用名 } - { 环境名 }.yml
    / { 分支名 } / { 应用名 } - { 环境名 }.properties
    
    
    乱码解决方案：https://www.cnblogs.com/zuowj/p/10432445.html
    
    config center 高可用配置（包含其他网络异常等情况）：https://blog.csdn.net/weixin_40470497/article/details/83780709
    
    ###SpringCloud使用Sentinel 代替 Hystrix
    https://blog.csdn.net/u011277123/article/details/92763625
    
    自建Sentinel与阿里云上Sentinel互相切换
    https://github.com/alibaba/Sentinel/wiki/AHAS-Sentinel-%E6%8E%A7%E5%88%B6%E5%8F%B0?spm=a2c4e.10696291.0.0.37a519a4YjKBDl
    
    Sentinel 实战-集群限流环境搭建
    https://www.jianshu.com/p/bb198c08b418
    
    ###Spring Cloud Zuul 网关服务整合Swagger2接口文档
    https://blog.csdn.net/Anenan/article/details/90177175
    
    ###Spring CLoud Zuul 网关切换不同服务接口文档访问异常
    https://blog.csdn.net/java_OnTheWay_kouhao/article/details/89841017
    
    ### SpringCloud访问http://xxx/actuator/health无法显示断路器信息无法显示
    首先需要引入actuator依赖
        <!--监控中心-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-actuator</artifactId>
            </dependency>
            
    监控中心健康检查
    management.endpoint.health.show-details=always
    
    ### springcloud  聚合监控(Hystrix Turbine)
    https://blog.csdn.net/chengyuqiang/article/details/92813469
    
    ### springcloud zipkin 最新版已无需自建项目
        下载地址：https://dl.bintray.com/openzipkin/maven/io/zipkin/java/zipkin-server/
        运行：java -jar zipkin-server-2.10.4-exec.jar
        访问：localhost:9411
        1 各微服务只需添加以下依赖：
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-zipkin</artifactId>
            </dependency>
            
        2 业务接口模块引用此包即可
        
     ### org.springframework.web.client.RestTemplate 异常 
     https://www.cnblogs.com/EasonJim/p/7546136.html