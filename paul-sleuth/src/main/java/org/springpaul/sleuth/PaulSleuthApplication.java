package org.springpaul.sleuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import zipkin2.server.internal.EnableZipkinServer;

/**
 * Created by zmk523@163.com on 2020/4/18 15:50
 */

@SpringBootApplication
@EnableDiscoveryClient
//@EnableZipkinServer: 开启zipkinServer的功能
@EnableZipkinServer
public class PaulSleuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(PaulSleuthApplication.class, args);
    }

}

