package org.springpaul.auth.granter;

import lombok.Data;
import org.springpaul.common.support.Kv;

/**
 * TokenParameter
 *
 * @author Chill
 */
@Data
public class TokenParameter {

	private Kv args = Kv.init();

}
