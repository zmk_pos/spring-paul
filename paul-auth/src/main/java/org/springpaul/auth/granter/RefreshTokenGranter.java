package org.springpaul.auth.granter;

import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import org.kjs.user.api.feign.IUserClient;
import org.springframework.stereotype.Component;
import org.kjs.user.api.vo.UserInfo;
import org.springpaul.common.constant.TokenConstant;
import org.springpaul.common.utils.Func;
import org.springpaul.common.utils.SecureUtil;

import java.util.Objects;

/**
 * RefreshTokenGranter
 *
 * @author Chill
 */
/*
@Component
@AllArgsConstructor
public class RefreshTokenGranter implements ITokenGranter {

    public static final String GRANT_TYPE = "refresh_token";

    private IUserClient userClient;

    @Override
    public UserInfo grant(TokenParameter tokenParameter) {
        String grantType = tokenParameter.getArgs().getStr("grantType");
        String refreshToken = tokenParameter.getArgs().getStr("refreshToken");
        UserInfo userInfo = null;
        if (Func.isNoneBlank(grantType, refreshToken) && grantType.equals(TokenConstant.REFRESH_TOKEN)) {
            Claims claims = SecureUtil.parseJWT(refreshToken);
            String tokenType = Func.toStr(Objects.requireNonNull(claims).get(TokenConstant.TOKEN_TYPE));
            if (tokenType.equals(TokenConstant.REFRESH_TOKEN)) {
                return userClient.userInfo(Func.toLong(claims.get(TokenConstant.USER_ID)));
            }
        }
        return userInfo;
    }
}
*/
