package org.springpaul.auth.granter;

import lombok.AllArgsConstructor;
import org.kjs.user.api.feign.IUserClient;
import org.springframework.stereotype.Component;
import org.kjs.user.api.vo.UserInfo;
import org.springpaul.auth.enums.PaulUserEnum;
import org.springpaul.common.utils.DigestUtil;
import org.springpaul.common.utils.Func;

/**
 * PasswordTokenGranter
 *
 * @author Chill
 */
@Component
@AllArgsConstructor
public class PasswordTokenGranter implements ITokenGranter {

    public static final String GRANT_TYPE = "password";

    private IUserClient userClient;

    @Override
    public UserInfo grant(TokenParameter tokenParameter) {
        String tenantId = tokenParameter.getArgs().getStr("tenantId");
        String account = tokenParameter.getArgs().getStr("account");
        String password = tokenParameter.getArgs().getStr("password");
        if (Func.isNoneBlank(account, password)) {
            // 获取用户类型
            String userType = tokenParameter.getArgs().getStr("userType");
            // 根据不同用户类型调用对应的接口返回数据，用户可自行拓展
            if (userType.equals(PaulUserEnum.WEB.getName())) {
                return userClient.userInfo(tenantId, account, password);
            } else if (userType.equals(PaulUserEnum.APP.getName())) {
                return userClient.userInfo(tenantId, account, DigestUtil.encrypt(password));
            } else {
                return userClient.userInfo(tenantId, account, DigestUtil.encrypt(password));
            }
        }
        return null;
    }

}
