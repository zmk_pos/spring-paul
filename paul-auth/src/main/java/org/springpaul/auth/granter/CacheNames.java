package org.springpaul.auth.granter;

/**
 * Created by zmk523@163.com on 2020/4/24 9:08
 */
public interface CacheNames {
    String CAPTCHA_KEY = "blade:auth::captcha:";
}
