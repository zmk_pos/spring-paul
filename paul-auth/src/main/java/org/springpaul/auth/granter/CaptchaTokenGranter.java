package org.springpaul.auth.granter;

import lombok.AllArgsConstructor;
import org.kjs.user.api.feign.IUserClient;
import org.springframework.stereotype.Component;
import org.kjs.user.api.vo.UserInfo;
import org.springpaul.auth.enums.PaulUserEnum;
import org.springpaul.auth.utils.TokenUtil;
import org.springpaul.common.api.R;
import org.springpaul.common.exception.ServiceException;
import org.springpaul.common.utils.*;
import org.springpaul.redis.RedisUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * 验证码TokenGranter
 *
 * @author Chill
 */
/*@Component
@AllArgsConstructor
public class CaptchaTokenGranter implements ITokenGranter {

	public static final String GRANT_TYPE = "captcha";

	private IUserClient userClient;
	private RedisUtil redisUtil;

	@Override
	public UserInfo grant(TokenParameter tokenParameter) {
		HttpServletRequest request = WebUtil.getRequest();

		String key = request.getHeader(TokenUtil.CAPTCHA_HEADER_KEY);
		String code = request.getHeader(TokenUtil.CAPTCHA_HEADER_CODE);
		// 获取验证码
		String redisCode = String.valueOf(redisUtil.get(CacheNames.CAPTCHA_KEY + key));
		// 判断验证码
		if (code == null || !StringUtil.equalsIgnoreCase(redisCode, code)) {
			throw new ServiceException(TokenUtil.CAPTCHA_NOT_CORRECT);
		}

		String tenantId = tokenParameter.getArgs().getStr("tenantId");
		String account = tokenParameter.getArgs().getStr("account");
		String password = tokenParameter.getArgs().getStr("password");
		if (Func.isNoneBlank(account, password)) {
			// 获取用户类型
			String userType = tokenParameter.getArgs().getStr("userType");
			R<UserInfo> result;
			// 根据不同用户类型调用对应的接口返回数据，用户可自行拓展
			if (userType.equals(PaulUserEnum.WEB.getName())) {
				return userClient.userInfo(tenantId, account, DigestUtil.encrypt(password));
			} else if (userType.equals(PaulUserEnum.APP.getName())) {
				return  userClient.userInfo(tenantId, account, DigestUtil.encrypt(password));
			} else {
				return userClient.userInfo(tenantId, account, DigestUtil.encrypt(password));
			}
		}
		return null;
	}

}*/
