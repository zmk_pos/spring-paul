package org.springpaul.auth;

import org.kjs.common.constant.AppConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by zmk523@163.com on 2020/4/18 15:04
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(AppConstant.BASE_PACKAGES)
@ComponentScan(basePackages = {"org.springpaul.redis","org.springpaul.auth","org.springpaul.common"})
public class PaulAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(PaulAuthApplication.class, args);
    }
}
