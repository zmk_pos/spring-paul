package org.sprignpaul.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by zmk523@163.com on 2020/4/18 10:25
 */

@EnableDiscoveryClient
@SpringBootApplication
public class PualDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PualDemoApplication.class, args);
    }

}