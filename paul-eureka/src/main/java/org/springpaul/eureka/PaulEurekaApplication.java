package org.springpaul.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class PaulEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaulEurekaApplication.class, args);
    }

}
