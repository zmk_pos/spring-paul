package org.kjs.user.service;

        import org.kjs.common.base.IBaseService;
        import org.kjs.user.api.vo.UserInfo;
        import org.kjs.user.api.pojo.SysUser;
        import org.kjs.user.api.pojo.SysUserExample;

/**
 * Created by zmk523@163.com on 2020/4/24 10:43
 */
public interface IUserService extends IBaseService<SysUser, SysUserExample> {

    UserInfo userInfo(Long tenantId, String userName, String password);
}
