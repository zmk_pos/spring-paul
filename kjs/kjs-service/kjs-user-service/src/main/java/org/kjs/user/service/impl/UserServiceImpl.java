package org.kjs.user.service.impl;

import org.kjs.common.base.BaseServiceImpl;
import org.kjs.user.api.vo.UserInfo;
import org.kjs.user.mapper.SysUserMapper;
import org.kjs.user.mapper.ext.SysUserExtMapper;
import org.kjs.user.api.pojo.SysUser;
import org.kjs.user.api.pojo.SysUserExample;
import org.kjs.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Created by zmk523@163.com on 2020/4/24 10:43
 */

@Repository
@Service
public class UserServiceImpl extends BaseServiceImpl<SysUserMapper, SysUser, SysUserExample> implements IUserService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysUserExtMapper sysUserExtMapper;
    

    @Override
    public UserInfo userInfo(Long tenantId, String userName, String password) {
        UserInfo userInfo = new UserInfo();
        SysUser sysUser = sysUserExtMapper.getUser(tenantId, userName, password);
        if (null == sysUser){
            return null;
        }
        userInfo.setUser(sysUser);

        return userInfo;
    }
}
