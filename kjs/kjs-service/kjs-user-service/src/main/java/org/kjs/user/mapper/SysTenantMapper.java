package org.kjs.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.kjs.user.api.pojo.SysTenant;
import org.kjs.user.api.pojo.SysTenantExample;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface SysTenantMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    int countByExample(SysTenantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    int deleteByExample(SysTenantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    int insert(SysTenant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    int insertSelective(SysTenant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    List<SysTenant> selectByExample(SysTenantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    SysTenant selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") SysTenant record, @Param("example") SysTenantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") SysTenant record, @Param("example") SysTenantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(SysTenant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_tenant
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(SysTenant record);
}