package org.kjs.user.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.kjs.user.api.feign.IUserClient;
import org.kjs.user.api.pojo.SysUser;
import org.kjs.user.api.vo.UserInfo;
import org.kjs.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zmk523@163.com on 2020/4/24 10:52
 */

@Api("用户接口")
@CrossOrigin
@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private IUserService userService;
    @Autowired
    private IUserClient userClient;

    @ApiOperation(value = "根据商品ID获取单条记录", notes = "根据商品ID获取单条记录", httpMethod = "GET")
    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public SysUser getById(@PathVariable("id") Long id) {
        return userService.selectByPrimaryKey(id);
    }

    @RequestMapping(value = "/testFeign/{id}", method = RequestMethod.GET)
    public SysUser testFeign(@PathVariable("id") Long id) {
        return userClient.getById(id);
    }

    @GetMapping("/get/info")
    public UserInfo userInfo(Long tenantId, String account, String password) {
        return userService.userInfo(tenantId, account, password);
    }
}