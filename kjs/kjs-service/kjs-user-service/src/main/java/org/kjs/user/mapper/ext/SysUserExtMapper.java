package org.kjs.user.mapper.ext;


import org.apache.ibatis.annotations.Mapper;
import org.kjs.user.api.pojo.SysUser;
import org.springframework.stereotype.Repository;

/**
 * Created by zmk523@163.com on 2020/4/24 13:44
 */

@Repository
@Mapper
public interface SysUserExtMapper {

    /**
     * 获取用户
     *
     * @param tenantId
     * @param userName
     * @param password
     * @return
     */
    SysUser getUser(Long tenantId, String userName, String password);


    /**
     * 
     * @param userName
     * @param password
     * @return
     */
    SysUser getUserByNameAndPwd(String userName, String password);
}
