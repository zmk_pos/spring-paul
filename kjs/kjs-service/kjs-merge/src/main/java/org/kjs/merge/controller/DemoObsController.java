package org.kjs.merge.controller;

import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.kjs.merge.service.DemoObsService;
import org.kjs.merge.service.DemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import rx.Observable;
import rx.Observer;

import java.util.HashMap;

/**
 * Created by zmk523@163.com on 2020/4/21 15:22
 */

@Api(description = "订单 接口")
@CrossOrigin
@RequestMapping("/order/obs")
@RestController
public class DemoObsController {
    public static final Logger logger = LoggerFactory.getLogger(DemoObsController.class);

    @Autowired
    private DemoObsService demoObsService;


    @ApiOperation(value = "测试接口", notes = "测试接口")
    @GetMapping("/get")
    public DeferredResult<HashMap<String, Object>> aggregate(
            @ApiParam("orderId") @RequestParam Long orderId,
            @ApiParam("productId") @RequestParam Long productId) {
        Observable<HashMap<String, Object>> result = this.aggregateObservable(orderId, productId);
        return this.toDeferredResult(result);
    }

    public Observable<HashMap<String, Object>> aggregateObservable(Long orderId, Long productId) {
        // 合并两个或者多个Observables发射出的数据项，根据指定的函数变换它们
        return Observable.zip(
                this.demoObsService.getProductById(productId),
                this.demoObsService.getOrderById(orderId),
                (product, orders) -> {
                    HashMap<String, Object> map = Maps.newHashMap();
                    map.put("product", product);
                    map.put("terminal", orders);
                    return map;
                }
        );
    }

    private DeferredResult<HashMap<String, Object>> toDeferredResult(Observable<HashMap<String, Object>> details) {
        DeferredResult<HashMap<String, Object>> result = new DeferredResult<>();
        // 订阅
        details.subscribe(new Observer<HashMap<String, Object>>() {
            @Override
            public void onCompleted() {
                logger.info("完成...");
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error("发生错误...", throwable);
            }

            @Override
            public void onNext(HashMap<String, Object> movieDetails) {
                result.setResult(movieDetails);
            }
        });
        return result;
    }
}
