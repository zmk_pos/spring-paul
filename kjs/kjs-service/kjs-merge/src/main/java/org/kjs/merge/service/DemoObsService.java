package org.kjs.merge.service;

import org.kjs.common.constant.AppConstant;
import org.kjs.merge.vo.Orders;
import org.kjs.merge.vo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import rx.Observable;


/**
 * Created by zmk523@163.com on 2020/4/21 15:15
 */

@Service
@Repository
public class DemoObsService {

    @Autowired
    private RestTemplate restTemplate;

    public Observable<Product> getProductById(Long id) {
        // 创建一个被观察者
        return Observable.create(observer -> {
            Product productVO = restTemplate.getForObject("http://" + AppConstant.APPLICATION_PRODUCT_NAME + "/product/get/{id}", Product.class, id);
            observer.onNext(productVO);
            observer.onCompleted();
        });
    }


    public Observable<Orders> getOrderById(Long id) {
        // 创建一个被观察者
        return Observable.create(observer -> {
            Orders productVO = restTemplate.getForObject("http://" + AppConstant.APPLICATION_ORDER_NAME + "/order/get/{id}", Orders.class, id);
            observer.onNext(productVO);
            observer.onCompleted();
        });
    }
}
