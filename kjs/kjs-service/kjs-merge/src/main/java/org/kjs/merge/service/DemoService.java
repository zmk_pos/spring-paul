package org.kjs.merge.service;

import org.kjs.order.api.feign.IOrderClient;
import org.kjs.order.api.vo.Orders;
import org.kjs.product.api.feign.IProductClient;
import org.kjs.product.api.vo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.util.HashMap;

/**
 * Created by zmk523@163.com on 2020/4/21 17:55
 */

@Service
@Repository
public class DemoService {

    @Autowired
    private IOrderClient orderClient;
    @Autowired
    private IProductClient productClient;


    public HashMap<String, Object> getOrderDetail(Long orderId, Long productId) {
        Object orders = orderClient.getById(orderId);
        Object product = productClient.getById(productId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("orders", orders);
        map.put("product", product);
        return map;
    }


    public Observable<Product> getProductById(Long id) {
        // 创建一个被观察者
        return Observable.create(observer -> {
            // 请求图表配置
            Product product = productClient.getById(id);
            observer.onNext(product);
            observer.onCompleted();
        });
    }

    public Observable<Orders> getOrderById(Long id) {
        // 创建一个被观察者
        return Observable.create(observer -> {
            // 请求图表配置
            Orders orders = orderClient.getById(id);
            observer.onNext(orders);
            observer.onCompleted();
        });
    }
}
