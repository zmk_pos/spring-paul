package org.kjs.order.service.impl;

import org.kjs.common.base.BaseServiceImpl;
import org.kjs.order.mapper.OrderProductMapper;
import org.kjs.order.pojo.OrderProduct;
import org.kjs.order.pojo.OrderProductExample;
import org.kjs.order.service.IOrderProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Created by zmk523@163.com on 2020/4/22 13:15
 */
@Repository
@Service
public class OrderProductServiceImpl extends BaseServiceImpl<OrderProductMapper, OrderProduct, OrderProductExample> implements IOrderProductService {
    @Autowired
    private OrderProductMapper orderProductMapper;
}
