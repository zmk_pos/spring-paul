package org.kjs.order.service;

import org.kjs.common.base.IBaseService;
import org.kjs.order.pojo.OrderProduct;
import org.kjs.order.pojo.OrderProductExample;

/**
 * Created by zmk523@163.com on 2020/4/22 13:15
 */
public interface IOrderProductService extends IBaseService<OrderProduct, OrderProductExample> {
}
