package org.kjs.order.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.kjs.order.api.feign.IOrderClient;
import org.kjs.order.pojo.Orders;
import org.kjs.order.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zmk523@163.com on 2020/4/20 15:54
 */
@Api(value = "order-controller", description = "订单 接口")
@CrossOrigin
@RequestMapping("/order")
@RestController
public class OrderController {

    @Autowired
    private IOrderService orderService;
    @Autowired
    private IOrderClient orderClient;


    @ApiOperation(value = "根据订单ID获取单条记录", notes = "根据订单ID获取单条记录", httpMethod = "GET")
    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public Orders getById(@PathVariable("id") Long id) {
        return orderService.selectByPrimaryKey(id);
    }


    @RequestMapping(value = "/testFeign/{id}", method = RequestMethod.GET)
    public Object testFeign(@PathVariable("id") Long id) {
        return orderClient.getById(id);
    }
}
