package org.kjs.order.service.impl;

import org.kjs.common.base.BaseServiceImpl;
import org.kjs.order.mapper.OrdersMapper;
import org.kjs.order.pojo.Orders;
import org.kjs.order.pojo.OrdersExample;
import org.kjs.order.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Created by zmk523@163.com on 2020/4/20 15:52
 */

@Repository
@Service
public class OrderServiceImpl extends BaseServiceImpl<OrdersMapper, Orders, OrdersExample> implements IOrderService {

    @Autowired
    private OrdersMapper ordersMapper;
}
