package org.kjs.order.service;

import org.kjs.common.base.IBaseService;
import org.kjs.order.pojo.Orders;
import org.kjs.order.pojo.OrdersExample;

/**
 * Created by zmk523@163.com on 2020/4/20 15:50
 */

public interface IOrderService extends IBaseService<Orders, OrdersExample> {
}
