package org.kjs.product.service.impl;

import org.kjs.common.base.BaseServiceImpl;
import org.kjs.product.mapper.ProductCategoryMapper;
import org.kjs.product.pojo.ProductCategory;
import org.kjs.product.pojo.ProductCategoryExample;
import org.kjs.product.service.IProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Created by zmk523@163.com on 2020/4/22 13:20
 */

@Repository
@Service
public class ProductCategoryServiceImpl extends BaseServiceImpl<ProductCategoryMapper, ProductCategory, ProductCategoryExample> implements IProductCategoryService {
    @Autowired
    private ProductCategoryMapper productCategoryMapper;
}
