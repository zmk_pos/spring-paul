package org.kjs.product.service;

import org.kjs.common.base.IBaseService;
import org.kjs.product.pojo.ProductImg;
import org.kjs.product.pojo.ProductImgExample;

/**
 * Created by zmk523@163.com on 2020/4/22 13:23
 */
public interface IProductImgService extends IBaseService<ProductImg, ProductImgExample> {
}
