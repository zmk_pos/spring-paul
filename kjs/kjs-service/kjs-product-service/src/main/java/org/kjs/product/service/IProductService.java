package org.kjs.product.service;

import org.kjs.common.base.IBaseService;
import org.kjs.product.pojo.Product;
import org.kjs.product.pojo.ProductExample;

/**
 * Created by zmk523@163.com on 2020/4/20 15:30
 */
public interface IProductService extends IBaseService<Product, ProductExample> {
}
