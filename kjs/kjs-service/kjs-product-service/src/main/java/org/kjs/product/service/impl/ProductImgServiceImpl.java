package org.kjs.product.service.impl;

import org.kjs.common.base.BaseServiceImpl;
import org.kjs.product.mapper.ProductImgMapper;
import org.kjs.product.pojo.ProductImg;
import org.kjs.product.pojo.ProductImgExample;
import org.kjs.product.service.IProductImgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Created by zmk523@163.com on 2020/4/22 13:24
 */
@Repository
@Service
public class ProductImgServiceImpl extends BaseServiceImpl<ProductImgMapper, ProductImg, ProductImgExample> implements IProductImgService {
    @Autowired
    private ProductImgMapper productImgMapper;
}
