package org.kjs.product.service;

import org.kjs.common.base.IBaseService;
import org.kjs.product.pojo.ProductCategory;
import org.kjs.product.pojo.ProductCategoryExample;

/**
 * Created by zmk523@163.com on 2020/4/22 13:19
 */
public interface IProductCategoryService extends IBaseService<ProductCategory, ProductCategoryExample> {
}
