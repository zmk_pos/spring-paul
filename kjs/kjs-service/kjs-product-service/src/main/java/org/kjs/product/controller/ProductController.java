package org.kjs.product.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.kjs.product.api.feign.IProductClient;
import org.kjs.product.pojo.Product;
import org.kjs.product.service.IProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by zmk523@163.com on 2020/4/20 15:34
 */

@Api(description = "产品 接口")
@CrossOrigin
@RequestMapping("/product")
@RestController
public class ProductController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private IProductService productService;
    @Autowired
    private IProductClient productClient;

    @ApiOperation(value = "根据商品ID获取单条记录", notes = "根据商品ID获取单条记录", httpMethod = "GET")
    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public Product getById(@PathVariable("id") Long id) {
        return productService.selectByPrimaryKey(id);
    }

    @RequestMapping(value = "/testFeign/{id}", method = RequestMethod.GET)
    public Object testFeign(@PathVariable("id") Long id) {
        return productClient.getById(id);
    }
}
