package org.kjs.product.service.impl;

import org.kjs.common.base.BaseServiceImpl;
import org.kjs.product.mapper.ProductMapper;
import org.kjs.product.pojo.Product;
import org.kjs.product.pojo.ProductExample;
import org.kjs.product.service.IProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Created by zmk523@163.com on 2020/4/20 15:30
 */

@Repository
@Service
public class ProductServiceImpl extends BaseServiceImpl<ProductMapper, Product, ProductExample> implements IProductService {
    private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductMapper productMapper;
}
