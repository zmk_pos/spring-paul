package org.kjs.order.api.feign;

import org.kjs.common.constant.AppConstant;
import org.kjs.order.api.vo.Orders;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by zmk523@163.com on 2020/4/20 11:02
 */

@FeignClient(
        value = AppConstant.APPLICATION_ORDER_NAME,
        path = "/order",
        fallback = IOrderClientFallback.class
)
public interface IOrderClient {

    @GetMapping("/get/{id}")
    Orders getById(@PathVariable("id") Long id);
}
