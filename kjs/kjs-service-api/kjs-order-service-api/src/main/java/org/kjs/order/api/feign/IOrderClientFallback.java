package org.kjs.order.api.feign;

import org.kjs.order.api.vo.Orders;
import org.springframework.stereotype.Component;

/**
 * Created by zmk523@163.com on 2020/4/20 11:02
 */

@Component
public class IOrderClientFallback implements IOrderClient {

    @Override
    public Orders getById(Long id) {
        return null;
    }
}
