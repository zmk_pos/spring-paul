package org.kjs.product.api.feign;

import org.kjs.product.api.vo.Product;
import org.springframework.stereotype.Component;

/**
 * Created by zmk523@163.com on 2020/4/20 11:02
 */

@Component
public class IProductClientFallback implements IProductClient {

    @Override
    public Product getById(Long id) {
        return null;
    }
}
