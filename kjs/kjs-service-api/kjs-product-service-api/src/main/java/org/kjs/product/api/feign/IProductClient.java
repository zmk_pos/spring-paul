package org.kjs.product.api.feign;

import org.kjs.common.constant.AppConstant;
import org.kjs.product.api.vo.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by zmk523@163.com on 2020/4/20 11:02
 */

@FeignClient(
        value = AppConstant.APPLICATION_PRODUCT_NAME,
        path = "/product",
        fallback = IProductClientFallback.class
)
public interface IProductClient {

    @GetMapping("/get/{id}")
    Product getById(@PathVariable("id") Long id);
}
