package org.kjs.user.api.feign;

import org.kjs.user.api.pojo.SysUser;
import org.kjs.user.api.vo.UserInfo;
import org.springframework.stereotype.Component;

/**
 * Created by zmk523@163.com on 2020/4/24 10:05
 */
@Component
public class IUserClientFallback implements IUserClient {
    @Override
    public SysUser getById(Long id) {
        return null;
    }

    @Override
    public UserInfo userInfo(Long userId) {
        return null;
    }

    @Override
    public UserInfo userInfo(String tenantId, String account, String password) {
        return null;
    }
}
