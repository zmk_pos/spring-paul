package org.kjs.user.api.feign;

import org.kjs.common.constant.AppConstant;
import org.kjs.user.api.pojo.SysUser;
import org.kjs.user.api.vo.UserInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by zmk523@163.com on 2020/4/24 10:05
 */
@FeignClient(
        value = AppConstant.APPLICATION_USER_NAME,
        path = "/user",
        fallback = IUserClientFallback.class
)
public interface IUserClient {

    @GetMapping("/get/{id}")
    SysUser getById(@PathVariable("id") Long id);


    /**
     * 获取用户信息
     *
     * @param userId 用户id
     * @return
     */
    @GetMapping("/user-info-by-id")
    UserInfo userInfo(@RequestParam("userId") Long userId);

    /**
     * 获取用户信息
     *
     * @param tenantId 租户ID
     * @param account  账号
     * @param password 密码
     * @return
     */
    @GetMapping("/get/info")
    UserInfo userInfo(@RequestParam("tenantId") String tenantId, @RequestParam("account") String account, @RequestParam("password") String password);

}