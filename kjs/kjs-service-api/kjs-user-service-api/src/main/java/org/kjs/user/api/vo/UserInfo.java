package org.kjs.user.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.kjs.user.api.pojo.SysUser;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zmk523@163.com on 2020/4/23 18:25
 */
@Data
@ApiModel(description = "用户信息")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户基础信息
     */
    @ApiModelProperty(value = "用户")
    private SysUser user;

    /**
     * 权限标识集合
     */
    @ApiModelProperty(value = "权限集合")
    private List<String> permissions;

    /**
     * 角色集合
     */
    @ApiModelProperty(value = "角色集合")
    private List<String> roles;

}