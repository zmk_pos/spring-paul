package org.kjs.common.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.kjs.common.enums.BaseCode;

import java.io.Serializable;

/**
 * Created by zmk523@163.com on 2019/11/4 12:03
 */

@ApiModel(value = "统一返回包格式")
public class BaseResult<T> implements Serializable {
    @ApiModelProperty(value = "业务状态码")
    private int code;   //返回码 非0即失败
    @ApiModelProperty(value = "描述")
    private String msg; //消息提示
    @ApiModelProperty(value = "对象")
    private T data; //返回的数据

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public BaseResult() {
    }

    public BaseResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = null;
    }

    public BaseResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }




    /**
     * 成功返回的json封装体
     * @param value 原始数据
     * @return json格式
     */
    public static BaseResult successJson(Object value){
        BaseResult results = new BaseResult();
        results.setCode(BaseCode.NORMAL.getCode());
        results.setMsg(BaseCode.NORMAL.getMessage());
        results.setData(value);
        return results;
    }

    /**
     * 失败返回的json封装体
     * @param msg
     * @return json格式
     */
    public static BaseResult errorJson(String msg){
        BaseResult results = new BaseResult();
        results.setCode(BaseCode.NOT_FOUND.getCode());
        results.setMsg(msg);
        return results;
    }
    
    /**
     * 失败返回的json封装体
     * @param msg
     * @param code
     * @return json格式
     */
    public static BaseResult errorJson(String msg,Integer code){
        BaseResult results = new BaseResult();
        results.setCode(code);
        results.setMsg(msg);
        return results;
    }

    
}