package org.kjs.common.constant;

/**
 * Created by zmk523@163.com on 2020/4/20 11:10
 */
public interface AppConstant {
    String BASE_PACKAGES = "org.kjs";
    String APPLICATION_USER_NAME = "user-service";
    String APPLICATION_PRODUCT_NAME = "product-service";
    String APPLICATION_ORDER_NAME = "order-service";
}
