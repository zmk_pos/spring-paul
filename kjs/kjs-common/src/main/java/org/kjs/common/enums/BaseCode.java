package org.kjs.common.enums;

/**
 * Created by zmk523@163.com on 2019/6/21 14:17
 */

public enum BaseCode {

    NORMAL("正常", 0),

    /***********************JWT BEGIN***********************/
    UNAUTHORIZED("未授权", 401),
    FORBIDDEN("禁止访问", 403),
    NOT_FOUND("没有找到", 404),
    NO_ACCESS("无权限", 1000),
    AUTH_LACK_OF("头部没有找到AUTHORIZATION的属性", 1001),
    AUTH_UNSUPPORTED("头部的AUTHORIZATION不支持", 1002),
    AUTH_JWT_EMPTY("头部的AUTHORIZATION中，缺少JWT的TOKEN", 1003),
    AUTH_JWT_SIGN("头部的AUTHORIZATION中，JWT的TOKEN签名不匹配", 1004),
    AUTH_JWT_EXPIRE("头部的AUTHORIZATION中，JWT的TOKEN已经失效", 1005),
    //没有找到AUTHORIZATION中的登录ID记录，需要重新登录
    AUTH_NEEDLOGIN("认证信息失效，请重新登陆", 1006),
    AUTH_FORMAT("头部的AUTHORIZATION的格式不正确", 1008),
    AUTH_SIGN_MISMATCH("签名不匹配", 1009),
    /***********************JWT END***********************/

    UNKNOWN_ERROR("业务数据错误", -1),
    DATA_ERROR("业务数据错误", 2000),
    DATA_NOT_FOUND("业务数据不存在", 2002),
    DATA_FORMATERROR("数据格式错误", 2003),
    DATA_TYPE_ERROR("数据类型错误", 2004),
    DATA_REPEAT("数据重复", 2005),
    DATA_UNSUPPORTED("数据没有授权", 2006),
    DATA_INVALID("数据无效", 2007),
    PARAME_TERERROR("参数错误", 3000),
    RESULT_ERROR("业务返回错误", 3006),
    REPEATED_REQUESTS("访问过于频繁，请稍后重试", 6009);

    private String message;
    private Integer code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }


    BaseCode(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

}
